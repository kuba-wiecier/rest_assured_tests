package HomeworkRestAssured;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class AddNewComment {
    String baseURL = "http://localhost:3000/comments";

    @Test
    void shouldReturnCode200WhenSavedNewPost() {
        Map<String, Object> postData = new HashMap<String, Object>();
        postData.put("title", "Example Title AssuredTest");
        postData.put("comment", "TestyAutomatyczne");

        given()
                .contentType(ContentType.JSON)
                .body(postData)
                .post(baseURL)
                .then()
                .statusCode(201);
    }
}
