package HomeworkRestAssured;

import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.when;

public class GetOneComment {
    String baseURL = "http://localhost:3000/comments";

    @Test
    void checkIfResponseStatusCodeIS200WhenGettingOneComment() {
        when().get(baseURL + "/3")
                .then().statusCode(200);
    }
}