package HomeworkRestAssured;

import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.when;

public class GetAllComments {
    String baseURL = "http://localhost:3000/comments";

    @Test
    void checkIfResponseStatusCodeIS200WhenGettingAllComments() {
        when().get(baseURL)
                .then().statusCode(200);
    }
}