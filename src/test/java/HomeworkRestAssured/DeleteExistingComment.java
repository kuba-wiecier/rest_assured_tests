package HomeworkRestAssured;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;

public class DeleteExistingComment {
    String baseURL = "http://localhost:3000/comments";

    @Test
    void deleteExistingPost() {
        Map<String, Object> postData = new HashMap<String, Object>();
        postData.put("title", "Example Title AssuredTest to delete");
        postData.put("comment", "Testy Automatyczne to delete");

        Integer id = given()
                .contentType(ContentType.JSON)
                .body(postData)
                .post(baseURL).body().jsonPath().get("id");

        when().delete(baseURL + "/" + id)
                .then().statusCode(200);
    }
}
