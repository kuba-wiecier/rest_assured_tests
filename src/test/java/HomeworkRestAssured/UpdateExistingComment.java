package HomeworkRestAssured;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.IsEqual.equalTo;

public class UpdateExistingComment {
    String baseURL = "http://localhost:3000/comments/";

    @Test
    void shouldBeAbleToEditARecord() {
        Map<String, Object> postData = new HashMap<String, Object>();
        postData.put("title", "example title");
        postData.put("comment", "example comment");

        Map<String, Object> updatedPostData = new HashMap<>(postData);
        updatedPostData.replace("comment", "Example Comment After Edit");


        int id = given()
                .contentType(ContentType.JSON)
                .body(postData)
                .when()
                .post(baseURL)
                .then()
                .body("title", equalTo("example title"))
                .and()
                .body("comment", equalTo("example comment"))
                .extract()
                .path("id");

        given()
                .contentType(ContentType.JSON)
                .body(updatedPostData)
                .when()
                .put(baseURL + id)
                .then()
                .body("title", equalTo("example title"))
                .and()
                .body("comment", equalTo("Example Comment After Edit"));
    }
}
